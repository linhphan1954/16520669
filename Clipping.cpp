#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
	RECT rect;
	rect.Left = l;
	rect.Right = r;
	rect.Top = t;
	rect.Bottom = b;

	return rect;
}

CODE Encode(RECT r, Vector2D P)
{
	CODE c = 0;
	if (P.x < r.Left)
		c = c | LEFT;
	if (P.x > r.Right)
		c = c | RIGHT;
	if (P.y < r.Top)
		c = c | TOP;
	if (P.y > r.Bottom)
		c = c | BOTTOM;
	return c;
}

int CheckCase(int c1, int c2)
{
	if (c1 == 0 && c2 == 0)
		return 1;
	if (c1 != 0 && c2 != 0 && c1&c2 != 0)
		return 2;
	return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	int c1 = Encode(r, P1);
	int c2 = Encode(r, P2);
	int th = CheckCase(c1, c2);
	while (th > 3) {
		ClippingCohenSutherland(r, P1, P2);
		c1 = Encode(r, P1);
		c2 = Encode(r, P2);
		th = CheckCase(c1, c2);
	}
	if (th == 2)
		return 0;
	Q1 = P1;
	Q2 = P2;
	return 1;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	if (Encode(r, P1) == 0) {
		Vector2D temp = P1;
		P1 = P2;
		P2 = temp;
	}
	int c1 = Encode(r, P1);
	int c2 = Encode(r, P2);
	int dx = P2.x - P1.x;
	int dy = P2.y - P1.y;
	float m;
	if (dx != 0)
		m = (float)dy / dx;
	if (c1&LEFT != 0)
	{
		P1.y += m * (r.Left - P1.x);
		P1.x = r.Left;
		return;
	}
	if (c1&RIGHT != 0) {
		P1.y += m * (r.Right - P1.x);
		P1.x = r.Right;
		return;
	}
	if (c1&TOP != 0) {
		if (dx != 0)
			P1.x += (r.Top - P1.y) / m;
		P1.y = r.Top;
		return;
	}
	if (c1&BOTTOM != 0) {
		if (dx != 0)
			P1.x += (r.Bottom - P1.y) / m;
		P1.y = r.Bottom;
	}
}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
	if (p == 0)
	{
		if (q < 0)
			return 0;
		return 1;
	}

	if (p > 0)
	{
		float t = (float)q / p;
		if (t2<t)
			return 1;
		if (t<t1)
			return 0;
		t2 = t;
		return 1;
	}

	float t = (float)q / p;
	if (t2<t)
		return 0;
	if (t<t1)
		return 1;
	t1 = t;
	return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	float tmin = 0;
	float tmax = 1;
	int dx = P2.x - P1.x;
	int dy = P2.y - P1.y;
	float p[4], q[4], t[4];
	p[0] = -dx;
	p[1] = dx;
	p[2] = -dy;
	p[3] = dy;
	q[0] = P1.x - r.Left;
	q[1] = r.Right - P1.x;
	q[2] = P1.y - r.Bottom;
	q[3] = r.Top - P1.y;
	for (int i = 0; i < 4; i++) {
		t[i] = SolveNonLinearEquation(p[i], q[i], tmin, tmax);
	}
	if (t[0] * t[1] * t[2] * t[3] == 0)
		return 0;
	tmin=t[0];
	for (int i = 0; i < 4; i++) {
		if (tmin >= t[i])
			tmin = t[i];
	}
	tmax = t[0];
	for (int j = 0; j < 4; j++) {
		if (tmin <= t[j])
			tmax = t[j];
	}
	Q1.x = P1.x + int(tmin*dx + 0.5);
	Q1.y = P1.y + int(tmin*dy + 0.5);
	Q2.x = P1.x + int(tmax*dx + 0.5);
	Q2.y = P1.y + int(tmax*dy + 0.5);
}
