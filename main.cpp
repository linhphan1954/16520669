// DoHoaMayTinhVaXuLiAnh.cpp : Defines the entry point for the console application.
//
// DoHoaMayTinhVaXuLiAnh.cpp : Defines the entry point for the console application.
//

// DoHoaMayTinhVaXuLiAnh.cpp : Defines the entry point for the console application.
//


#include <iostream>
#include <SDL.h>
#include "Bezier.h"

using namespace std;
SDL_Event event;
const int WIDTH = 800;
const int HEIGHT = 1000;


int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	Vector2D p1(22, 215), p2(94, 43), p3(140, 258), p4(213, 150);


	SDL_Rect *rect1 = new SDL_Rect();
	rect1->x = p1.x - 5;
	rect1->y = p1.y - 5;
	rect1->w = 10;
	rect1->h = 10;

	SDL_Rect *rect2 = new SDL_Rect();
	rect2->x = p2.x - 5;
	rect2->y = p2.y - 5;
	rect2->w = 10;
	rect2->h = 10;

	SDL_Rect *rect3 = new SDL_Rect();
	rect3->x = p3.x - 5;
	rect3->y = p3.y - 5;
	rect3->w = 10;
	rect3->h = 10;

	SDL_Rect *rect4 = new SDL_Rect();
	rect4->x = p4.x - 5;
	rect4->y = p4.y - 5;
	rect4->w = 10;
	rect4->h = 10;

	SDL_Color colorCurve = { 50, 150, 150, 255 }, colorRect = { 0, 255, 40, 255 };

	SDL_SetRenderDrawColor(ren, 50, 150, 150, 255);
	DrawCurve3(ren, p1, p2, p3, p4);
	SDL_RenderPresent(ren);
	SDL_SetRenderDrawColor(ren, 0, 255, 40, 255);
	SDL_RenderDrawRect(ren, rect1);
	SDL_RenderDrawRect(ren, rect2);
	SDL_RenderDrawRect(ren, rect3);
	SDL_RenderDrawRect(ren, rect4);
	SDL_RenderPresent(ren);

	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	bool running = true;
	bool inside = false;
	bool check = false;
	int x, y, i = 0;
	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{
			//HANDLE MOUSE EVENTS!!!


			if (event.type == SDL_MOUSEBUTTONDOWN & inside == false) {
				SDL_GetMouseState(&x, &y);
				if (event.type == SDL_MOUSEBUTTONDOWN) {
					for (i = 0; i < 4; i++) {
						Vector2D P;
						if (i == 0)  P = p1;
						if (i == 1)  P = p2;
						if (i == 2)  P = p3;
						if (i == 3)  P = p4;
						if ((x > P.x - 5) & (x < P.x + 5) & (y > P.y - 5) & (y < P.y + 5)) {
							inside = true;
							check = true;
							break;
						}
						if (i == 3) {
							inside = false;
							check = false;
						}
					}
				}

			}
			if (event.type == SDL_MOUSEMOTION) { continue; }
			if (event.type == SDL_MOUSEBUTTONUP)

			if (check) {
				inside = false;
				SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
				SDL_RenderClear(ren);
				SDL_SetRenderDrawColor(ren, 50, 150, 150, 255);
				SDL_GetMouseState(&x, &y);
				if (i == 0) {
					p1.x = x;
					p1.y = y;
				}
				if (i == 1) {
					p2.x = x;
					p2.y = y;
				}
				if (i == 2) {
					p3.x = x;
					p3.y = y;
				}
				if (i == 3) {
					p4.x = x;
					p4.y = y;
				}
				rect1->x = p1.x - 5;
				rect1->y = p1.y - 5;
				rect1->w = 10;
				rect1->h = 10;


				rect2->x = p2.x - 5;
				rect2->y = p2.y - 5;
				rect2->w = 10;
				rect2->h = 10;


				rect3->x = p3.x - 5;
				rect3->y = p3.y - 5;
				rect3->w = 10;
				rect3->h = 10;


				rect4->x = p4.x - 5;
				rect4->y = p4.y - 5;
				rect4->w = 10;
				rect4->h = 10;

				SDL_SetRenderDrawColor(ren, 0, 255, 40, 255);
				SDL_RenderDrawRect(ren, rect1);
				SDL_RenderDrawRect(ren, rect2);
				SDL_RenderDrawRect(ren, rect3);
				SDL_RenderDrawRect(ren, rect4);
				SDL_SetRenderDrawColor(ren, 50, 150, 150, 255);
				DrawCurve3(ren, p1, p2, p3, p4);
				SDL_RenderPresent(ren);
			}

			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
		}

	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
