#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3], y[3];
	float phi = 3.14 / 2;
	for (int i = 0; i < 3; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * 3.14 / 3;
	}
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	for (int i = 0; i < 3; i++) {
		Bresenham_Line(x[i], y[i], x[(i + 1) % 3], y[(i + 1) % 3], ren);
	}
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4], y[4];
	float phi = 3.14 / 4;
	for (int i = 0; i < 4; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 3.14 / 2;
	}
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	for (int i = 0; i < 4; i++) {
		Bresenham_Line(x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4],ren);
	}
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = 3.14 / 2;
	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2*3.14 / 5;
	}
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	for (int i = 0; i < 5; i++) {
		Bresenham_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6], y[6];
	float phi = 3.14 / 6;
	for (int i = 0; i < 6; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 3.14 / 3;
	}
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	for (int i = 0; i < 6; i++) {
		Bresenham_Line(x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6], ren);
	}
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = 3.14 / 2;
	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * 3.14 / 5;
	}
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	for (int i = 0; i < 5; i++) {
		Bresenham_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
	}
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5], xp[5], yp[5];
	float phi = 3.14 / 2;
	float rp = R * sin(3.14 / 10) / sin(7 * 3.14 / 10);
	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xp[i] = xc + int(rp*cos(phi + 3.14 / 5) + 0.5);
		yp[i] = yc - int(rp*sin(phi + 3.14 / 5) + 0.5);
		phi += 2 * 3.14 / 5;
	}
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	for (int i = 0; i < 5; i++) {
		Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
		Bresenham_Line(xp[i], yp[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], y[8];
	int xp[8], yp[8];
	float phi = 0;
	float rp = R * sin(3.14 / 8) / sin(6 * 3.14 / 8);
	for (int i = 0; i < 8; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xp[i] = xc + int(rp*cos(phi + 3.14 / 8) + 0.5);
		yp[i] = yc - int(rp*sin(phi + 3.14 / 8) + 0.5);
		phi += 2 * 3.14 / 8;
	}
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	for (int i = 0; i < 8; i++) {
		Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
		Bresenham_Line(xp[i], yp[i], x[(i + 1) % 8], y[(i + 1) % 8], ren);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5], y[5], xp[5], yp[5];
	float phi = startAngle;
	float rp = R * sin(3.14 / 10) / sin(7 * 3.14 / 10);
	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xp[i] = xc + int(rp*cos(phi + 3.14 / 5) + 0.5);
		yp[i] = yc - int(rp*sin(phi + 3.14 / 5) + 0.5);
		phi += 2 * 3.14 / 5;
	}
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	for (int i = 0; i < 5; i++) {
		Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
		Bresenham_Line(xp[i], yp[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	float bk = r;
	float gbd = 3.14 / 2;
	while (bk > 1) {
		DrawStarAngle(xc, yc, int(bk + 0.5), gbd, ren);
		bk = bk * sin(3.14 / 10) / sin(7 * 3.14 / 10);
		gbd = gbd + 3.14;
	}
}
