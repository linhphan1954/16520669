#include "Ellipse.h"
#include<math.h>

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x1 = 0;
	int y1 = b;
	int p1 = -2 * pow(a, 2)*b + pow(a, 2) + 2 * pow(b, 2);
	Draw4Points(xc, yc, x1, y1, ren);
	while (x1 < (pow(a, 2)) / sqrt(pow(a, 2) + pow(b, 2)))
	{
		if (p1 < 0) p1 += + 4 * pow(b, 2)*x1 + 6 * pow(b, 2);
		else
		{
			y1--;
			p1 += + 4 * pow(b, 2)*x1 - 4 * pow(a, 2)*y1;
		}
		Draw4Points(xc, yc, x1, y1, ren);
		x1 = x1 + 1;
	}
	// Area 2
	int x2 = a;
	int y2 = 0;
	int p2 = pow(b, 2) + 2 * pow(a, 2) - 2 * a*pow(b, 2);
	Draw4Points(xc, yc, x2, y2, ren);
	while (y2 < (pow(b, 2)) / sqrt(pow(a, 2) + pow(b, 2)))
	{
		if (p2 < 0) p2 += + 4 * pow(a, 2)*y2 + 6 * pow(a, 2);
		else
		{
			x2--;
			p2 += 4 * pow(a, 2)*y2 - 4 * pow(b, 2)*x2;
		}
		Draw4Points(xc, yc, x2, y2, ren);
		y2 = y2 + 1;
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x1 = 0;
	int y1 = b;
	float p1 = pow(b, 2) - pow(a, 2)*b + (pow(a, 2) / 4);
	Draw4Points(xc, yc, x1, y1, ren);
		while (pow(x1,2)*(pow(a,2) + pow(b,2)) <= pow(a,4))
		{
			if(p1<=0)
				p1 += 2 * pow(b, 2)*x1 + 3 * pow(b, 2);
			else
			{
				p1 += 2 * pow(b, 2)*x1 - 2 * pow(a, 2)*y1 + 3 * pow(b, 2) + 2 * pow(a, 2);
				y1 -= 1;
			}
			x1++;
			Draw4Points(xc, yc, x1, y1, ren);
		}
	// Area 2
	int x2 = a;
	int y2 = 0;
	float p2= pow(a, 2) - a*pow(b, 2) + (pow(b, 2) / 4);
	Draw4Points(xc, yc, x2, y2, ren);
	while (pow(x2, 2)*(pow(a,2) + pow(b,2)) > pow(a,4))
	{
		if(p2 <= 0)
			p2 += pow(a,2)*(2 * y2 + 3);
		else
		{
			p2 += -pow(b,2)*(2 * x2 - 2) + pow(a,2)*(2 * y2 + 3);
			x2 -= 1;
		}
		y2++;
		Draw4Points(xc, yc, x2, y2, ren);
	}
}
