#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	//draw 2 points
}

void Draw2PointsParapolNegative(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
}

void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x1 = 0;
	int y1 = 0;
	int p1 = 1 - A;
	Draw2Points(xc, yc, x1, y1, ren);
	while (x1 < A)
	{
		if (p1 <= 0) p1 = p1 + 2 * x1 + 3;
		else
		{
			y1++;
			p1 = p1 + 2 * x1 + 3 - 2 * A;
		}
		Draw2Points(xc, yc, x1, y1, ren);
		x1++;
	}

	int x2 = A;
	int y2 = A/2;

	int p2 = 2 * A - 1;
	Draw2Points(xc, yc, x2, y2, ren);
	while (y2 < 600)
	{
		if (p2 <= 0) p2 = p2 + 4 * A;
		else
		{
			x2++;
			p2 = p2 + 4 * A - 4 * x2 - 4;
		}
		Draw2Points(xc, yc, x2, y2, ren);
		y2++;
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x1 = 0;
	int y1 = 0;
	int p1 = 1 - A;
	Draw2PointsParapolNegative(xc, yc, x1, y1, ren);
	while (x1 < A)
	{
		if (p1 <= 0) p1 = p1 + 2 * x1 + 3;
		else
		{
			y1++;
			p1 = p1 + 2 * x1 + 3 - 2 * A;
		}
		Draw2PointsParapolNegative(xc, yc, x1, y1, ren);
		x1++;
	}

	int x2 = A;
	int y2 = A/2;
	int p2 = 2 * A - 1;
	Draw2PointsParapolNegative(xc, yc, x2, y2, ren);

	while (y2 < 600)
	{
		if (p2 <= 0) p2 = p2 + 4 * A;
		else
		{
			x2++;
			p2 = p2 + 4 * A - 4 * x2 - 4;
		}
		Draw2PointsParapolNegative(xc, yc, x2, y2, ren);
		y2++;
	}
}
